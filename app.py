from datetime import datetime 

def get_current_time():
    datetime_now = datetime.now() 
    current_time = datetime_now.strftime("%H:%M:%S") 
    return current_time

time_now = get_current_time()

print(time_now)